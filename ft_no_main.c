/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_no_main.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 11:14:30 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/11 11:15:50 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
** ft_2_map(), here we mmap() a TINY and SMALL page
** on malloc initialisation to not call mmap at
** at every succesive ptr = mallo(), free(ptr)
*/

t_page			*ft_2_map(void)
{
	t_page		*page;

	page = ft_add_page(TINY_BLOCK - sizeof(t_page) - META_SIZE);
	page->next = ft_add_page(SMALL_BLOCK - sizeof(t_page) - META_SIZE);
	return (page);
}

/*
** loop on every block in page given
*/

t_block			*ft_while_page(t_page *page, int size)
{
	t_block		*block_list;

	block_list = (void *)(page + 1);
	while (block_list->next != NULL)
	{
		if (block_list->free == 1 && (ft_size_block(block_list) >=
					(unsigned int)(size + META_SIZE)))
			return (block_list);
		block_list = block_list->next;
	}
	return (block_list);
}

/*
** char *ret must be 'INT' OR 'CHAR'
*/

char			*ft_page_type(t_page *page, char *ret)
{
	if (page->size == TINY_SIZE)
	{
		if (ft_strcmp(ret, "INT") == 0)
			return ("1");
		else if (ft_strcmp(ret, "CHAR") == 0)
			return ("TINY");
	}
	else if (page->size == SMALL_SIZE)
	{
		if (ft_strcmp(ret, "INT") == 0)
			return ("2");
		else if (ft_strcmp(ret, "CHAR") == 0)
			return ("SMALL");
	}
	else
	{
		if (ft_strcmp(ret, "INT") == 0)
			return ("3");
		else if (ft_strcmp(ret, "CHAR") == 0)
			return ("LARGE");
	}
	return (NULL);
}

/*
** Calcul size of given block with block
** by substracing address
*/

unsigned int	ft_size_block(void *block)
{
	t_block		*block_list;
	t_page		*page;
	int			size;

	block_list = block;
	if ((page = ft_find_page_block(block)) != NULL)
	{
		if (block_list != NULL)
		{
			if (block_list->next != NULL)
				size = (void *)block_list->next - block;
			else
				size = page->size - ((block - (void *)page + page->size_left));
			if (size >= 0)
				return (size);
			else
				return (-1);
		}
	}
	return (0);
}
