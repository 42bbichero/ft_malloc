/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/21 14:07:46 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/11 11:19:38 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H

# include <sys/mman.h>
# include <pthread.h>
# include <limits.h>
# include <string.h>
# include <stdio.h>
# include "../libft/libft.h"

/*
** Define page size
** TINY (smallest page, 16384 bytes)
** SMALL (middle page, 40960 bytes)
*/
# define TINY_SIZE (unsigned int)(PAGE_SIZE * 16)
# define TINY_BLOCK (TINY_SIZE / 10)
# define SMALL_SIZE (unsigned int)(PAGE_SIZE * 32)
# define SMALL_BLOCK (SMALL_SIZE / 10)
# define TINY 1
# define SMALL 2
# define LARGE 3
# define SIZE_BLOCK (int)(page->size / 10)

typedef struct		s_block
{
	int				free;
	struct s_block	*next;
}					t_block;

typedef struct		s_page
{
	unsigned int	size;
	unsigned int	full;
	unsigned int	size_left;
	void			*prev;
	void			*next;
}					t_page;

# define META_SIZE (int)sizeof(t_block)
# define PAGE_SIZE (int)getpagesize()

t_page *g_mem;

extern t_page *g_mem;
pthread_mutex_t	g_lock;

int					ft_page_reclaim(void);

void				ft_header_info(void *block, unsigned int size);
void				ft_while_block(t_block *block_list);
void				show_alloc_mem();

void				ft_fusion_block(void *cur);
void				free(void *ptr);
void				ft_free_2(void *ptr);
void				ft_move_block(void *free_block);
void				*ft_find_page_block(void *block);
void				ft_munmap_page(int free, t_page *page);

/*
** New malloc with struct for each pages size
*/
void				*malloc(size_t size);
t_page				*ft_2_map(void);
void				*ft_add_page(unsigned int size);
t_page				*ft_new_page(unsigned int size);
void				*ft_new_block(void *block, unsigned int size);
void				*ft_find_block_size(int size);
unsigned int		ft_size_block(void *block);
t_block				*ft_while_page(t_page *page, int size);

void				*realloc(void *ptr, size_t size);
char				*ft_page_type(t_page *page, char *ret);

#endif
