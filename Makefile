# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bbichero <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/07/26 11:07:22 by bbichero          #+#    #+#              #
#    Updated: 2017/07/26 11:25:03 by bbichero         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAMEO = libft_malloc
NAMELK = $(NAMEO).so
NAME = $(NAMEO)_$(HOSTTYPE).so

# LIBFT
LFTPATH = ../libft
LFTFILE = $(LFTPATH)/libft.a

OBJPATH = obj
SRCPATH = .

INCLUDE = -I ./

CC = gcc
SHARED = -shared 
BASEFLAGS = -Wall -Wextra
CFLAGS = $(BASEFLAGS) -Werror -O2 -g

MALLOC_FILES = 	ft_no_main.c \
				ft_block.c \
				ft_show_alloc_mem.c \
				ft_free.c \
				ft_realloc.c

# MALLOC.SO
SRC_MALLOC = $(addprefix $(SRCPATH)/,$(MALLOC_FILES))
MALLOC_OBJECTS = $(SRC_MALLOC:$(SRCPATH)/%.c=$(OBJPATH)/%.o)

RM = rm -rf

# Yellow
Y = \033[0;33m
# Red
R = \033[0;31m
# Green
G = \033[0;32m
# No color
E = \033[39m

all: libft $(NAME)

$(NAME): $(MALLOC_OBJECTS)
	ln -sf $(NAME) $(NAMELK)
	@echo "$(Y)[COMPILING MALLOC] $(G) $(CC) -o $@ $(CFLAGS) -g $(MALLOC_OBJECTS) $(INCLUDES) $(LFTFILE) $(E)"
	@$(CC) -shared -o $@ $(CFLAGS) -g $(MALLOC_OBJECTS) $(INCLUDES) $(LFTFILE)
	@echo "$(Y)[COMPILING MALLOC DONE]$(E)"

$(MALLOC_OBJECTS): $(OBJPATH)/%.o : $(SRCPATH)/%.c
	@mkdir -p $(dir $@)
	$(CC) -o $@ $(CFLAGS) $(INCLUDES) -c $<

clean:
	@echo "$(Y)[MALLOC]$(E)"
	@echo "$(R)[REMOVE OBJ DIR]$(E)"
	@$(RM) $(OBJPATH)
	@cd $(LFTPATH) && $(MAKE) -s clean

fclean: clean
	@$(RM) $(NAME)
	@$(RM) $(NAMELK)
	@cd $(LFTPATH) && $(MAKE) -s fclean

libft:
	@cd $(LFTPATH) && $(MAKE)

re: fclean all
