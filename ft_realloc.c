/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 11:20:09 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/11 11:34:51 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
** realloc() function, take 2 parameters
** void *ptr : pointer of block to resize
** size_t size : new size wanted
** realloc() try to change the size pointing
** by ptr by size
** If place after ptr, use ptr
** If not new allocation and leave ptr in place
** If size NULL, new allocation
*/

void			*realloc(void *ptr, size_t size)
{
	t_block		*block;
	int			block_size;

	block = ptr - META_SIZE;
	block_size = ft_size_block(block);
	if (ptr == NULL || size == 0 || !block)
		return (malloc(size));
	if (block_size == 0)
		return (NULL);
	else
	{
		if ((int)(size + META_SIZE) > block_size)
		{
			pthread_mutex_lock(&g_lock);
			block = malloc(size);
			block = ft_memcpy(block, ptr,
					ft_size_block(ptr - META_SIZE));
			pthread_mutex_unlock(&g_lock);
		}
	}
	return ((void *)block);
}
