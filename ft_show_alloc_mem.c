/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show_alloc_mem.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/26 14:38:57 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/11 11:35:15 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
** ft_size_block() send us total size block (with sizeof(t_block))
*/

void			ft_header_info(void *block, unsigned int size)
{
	ft_atoi_hex(block, 0);
	ft_putstr(" - ");
	ft_atoi_hex(block + size, 0);
	ft_putstr(" : ");
	ft_putnbr(size);
	ft_putstr(" bytes\n");
}

void			ft_while_block(t_block *block_list)
{
	while (block_list != NULL)
	{
		if (block_list->free == 0)
			ft_header_info(block_list, ft_size_block(block_list));
		block_list = block_list->next;
	}
}

/*
** show_alloc_mem() while on all page created
** and print information about each block
** page type, page start, block start and end
*/

void			show_alloc_mem(void)
{
	t_block		*block_list;
	t_page		*page;

	page = g_mem;
	pthread_mutex_lock(&g_lock);
	if (g_mem && page + 1)
	{
		while (page != NULL)
		{
			ft_putstr(ft_page_type(page, "CHAR"));
			ft_putstr(" - ");
			ft_atoi_hex(page, 1);
			block_list = (void *)(page + 1);
			if (block_list + 1)
				ft_while_block(block_list);
			page = page->next;
		}
	}
	pthread_mutex_unlock(&g_lock);
}
