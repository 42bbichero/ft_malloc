/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 20:02:12 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/11 11:31:34 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
** Call of munmap page when every block of a page is free
*/

void			ft_munmap_page(int free, t_page *page)
{
	t_page		*new;
	static int	i = 0;

	new = NULL;
	if (free == 1 && page != g_mem && page != g_mem->next)
	{
		new = page->prev;
		if (page->next)
			new->next = page->next;
		else
			new->next = NULL;
		if (munmap(page, page->size) == -1)
			ft_putendl("Error when trying to call munmap()");
		i++;
		page = NULL;
	}
}

/*
** When every block in a page are free, munmap
*/

void			ft_remove_page(void *ptr)
{
	t_page		*page;
	t_block		*block;
	int			free;

	free = 1;
	page = ptr;
	if (page + 1)
	{
		block = (void *)(page + 1);
		while (block != NULL)
		{
			if (block->free == 0)
				free = 0;
			block = block->next;
		}
		ft_munmap_page(free, page);
	}
}

/*
** Find page addr corresponding to given block
*/

void			*ft_find_page_block(void *block)
{
	t_block		*block_list;
	t_page		*page;

	page = g_mem;
	while (page != NULL)
	{
		block_list = (void *)(page + 1);
		while (block_list != NULL)
		{
			if (block_list == block)
				return (page);
			block_list = block_list->next;
		}
		page = page->next;
	}
	return (NULL);
}

/*
** Permit to free a certain block First find the concern
** page, and put free int value from bloc to 1
*/

void			free(void *ptr)
{
	t_block		*block;
	t_block		*next;
	t_page		*page;

	page = ft_find_page_block(ptr - META_SIZE);
	pthread_mutex_lock(&g_lock);
	if (page && ptr && (ptr - META_SIZE))
	{
		block = (void *)(page + 1);
		while (block != NULL)
		{
			if ((block + 1) == ptr)
			{
				block->free = 1;
				next = block->next;
				if (next != NULL && next->free == 1)
					ft_fusion_block(block);
				ft_remove_page(page);
				break ;
			}
			block = block->next;
		}
	}
	pthread_mutex_unlock(&g_lock);
}

/*
** When a block and is follower are free, we fusion pointer to
** make an unique pointer
*/

void			ft_fusion_block(void *cur)
{
	t_block		*b_cur;
	t_block		*b_next;

	b_cur = cur;
	b_next = b_cur->next;
	if (b_next && b_next->free == 1)
		*b_cur = *b_next;
	if (b_next && b_next->next == NULL)
		b_cur->next = NULL;
}
