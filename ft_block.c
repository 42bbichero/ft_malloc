/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_block.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 20:02:24 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/11 11:17:21 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

t_page *g_mem;

/*
** Malloc initialisation function
** Call when first malloc
** Initialise first page
** Initialise g_mem (page)
*/

void			*ft_add_page(unsigned int size)
{
	void		*new_page;

	if ((size + sizeof(t_page) + META_SIZE) <= (unsigned int)TINY_BLOCK)
		new_page = ft_new_page(TINY_SIZE);
	else if ((size + sizeof(t_page) + META_SIZE) <=
						(unsigned int)SMALL_BLOCK)
		new_page = ft_new_page(SMALL_SIZE);
	else
		new_page = ft_new_page(size + META_SIZE + sizeof(t_page));
	return (new_page);
}

/*
** Create new page with mmap() folowing page type given
** new_page contain :
**   - size_left : start with total size - sizeof(t_page)
**                 and decrease with block allocation
**   - size      : contain total size available for block allocation
**   - full      : 1 if page full or 0
**   - next      : pointer to next page
*/

t_page			*ft_new_page(unsigned int size)
{
	t_page		*new_page;

	new_page = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_ANON |
							MAP_PRIVATE, -1, 0);
	if (new_page == MAP_FAILED)
		return ((t_page *)-1);
	new_page->size_left = size - sizeof(t_page);
	new_page->size = size;
	new_page->full = 0;
	new_page->next = NULL;
	return (new_page);
}

/*
** Search throw bock list a block containing at least
** (int)size + sizeof(t_block) space available
** If block come to block_list end, function return
** last but block
*/

void			*ft_find_block_size(int size)
{
	t_block		*block_list;
	t_page		*page;
	t_page		*new_page;

	page = g_mem;
	block_list = (void *)(page + 1);
	new_page = NULL;
	while (page != NULL)
	{
		if (SIZE_BLOCK >= (size + META_SIZE) && ((int)page->size_left
											>= (size + META_SIZE)))
		{
			block_list = ft_while_page(page, size);
			return ((void *)block_list);
		}
		if (page->next == NULL)
			break ;
		page = page->next;
	}
	if ((new_page = ft_add_page(size)) == (void *)-1)
		return ((void *)-1);
	new_page->prev = page;
	page->next = new_page;
	return ((void *)(page->next + sizeof(t_page)));
}

/*
** Take a start void pointer and size check
** if size variable is contain in block pointer
** and add a next pointer with allocated memory
*/

void			*ft_new_block(void *block, unsigned int size)
{
	t_page		*page;
	t_block		*new_block;
	char		*next;

	page = ft_find_page_block(block);
	new_block = block;
	if (ft_size_block(new_block) > 0)
	{
		next = (char *)(block) + ft_size_block(block);
		new_block->next = (void *)next;
		new_block = new_block->next;
	}
	new_block->next = NULL;
	new_block->free = 0;
	page->size_left -= (size + META_SIZE);
	return (new_block);
}

/*
** First, determine block type (tiny, small, large)
** then, see in concern struct
*/

void			*malloc(size_t size)
{
	t_block		*block_list;

	block_list = NULL;
	pthread_mutex_lock(&g_lock);
	if (g_mem)
	{
		if ((block_list = ft_find_block_size(size)) != (void *)-1)
		{
			if (block_list->free == 1)
				block_list->free = 0;
			else
				block_list = ft_new_block(block_list, size);
		}
		else
			return (NULL);
	}
	else
	{
		g_mem = ft_2_map();
		if (g_mem == (void *)-1)
			return (NULL);
		block_list = ft_new_block(g_mem + 1, size);
	}
	pthread_mutex_unlock(&g_lock);
	return ((void *)(block_list + 1));
}
